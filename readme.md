
# K8s Helm Chart for Microservices
Create Helm Chart for Microservices.

## Technologies Used
- Kubernetes
- Helm

## Project Description
- Create 1 shared Helm Chart for all microservices, to reuse common Deployment and Service configurations for the services

## Prerequisites
- [Minkube](https://minikube.sigs.k8s.io/docs/start/)
- [Kubectl](https://kubernetes.io/docs/tasks/tools/)
- [Helm](https://helm.sh/docs/intro/install/)
- [Previous Projects](https://gitlab.com/devops-public-projects/kubernetes/m10-6-microservice-app-k8s-deploy/-/blob/main/config.yaml?ref_type=heads) `config.yaml`

## Guide Steps
### Initial Configuration
- Create a basic template
	- `helm create microservices`
- For this demo we will clear out the templates directory and leave `deployment.yaml` and `service.yaml`. 
- We will also clear out the contents of these two files and the `values.yaml` and configure them all from scratch.

### Create Basic Template Files
- We will use the Deployment and Service template from [this repositories](https://gitlab.com/twn-devops-bootcamp/latest/10-kubernetes/helm-chart-microservices/-/blob/starting-code/config.yaml?ref_type=heads) `config.yaml`
	- Paste the respective content into each file
- `deployment.yaml`
	- We want to reuse as much as possible so we will configure every Deployment to have variables that reference Values (`values.yaml`)
```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ .Values.appName }}
spec:
  selector:
    matchLabels:
      app: {{ .Values.appName }}
  template:
    metadata:
      labels:
        app: {{ .Values.appName }}
    spec:
      containers:
      - name: {{ .Values.appName }}
        image: “{{ .Values.appImage }}:{{ .Values.appVersion }}”
        ports:
        - containerPort: {{ .Values.containerPort }}

```

- `service.yaml` 
	- Same as `deployment.yaml`, we will utilize variables instead of direct hard-coded values
```yaml
apiVersion: v1
kind: Service
metadata:
  name: {{ .Values.appName }}
spec:
  type: {{ .Values.serviceType }}
  selector:
    app: {{ .Values.appName }}
  ports:
  - protocol: TCP
    port: {{ .Values.servicePort }}
    targetPort: {{ .Values.containerPort }}

```

###  Dynamic Environment Variables
- All of our Microservices require at least one environment variable. We can add this to the container spec under containerPort for the `deployment.yaml`.
	- We will utilize a range to iterate over all possible envVars for a particular Microservice
```yaml
env:
{{- range .Values.containerEnvVars }}
 name: {{ .name }} #We use name in the values template
 value: {{ .value | quote }}
{{- end }}
```

### Creating Values Template
- We'll add some default values based on the names we set in `deployment.yaml` and `service.yaml`

```yaml
appName: servicename
appImage: gcr.io/google-samples/micrcoservices-demo/servicename
appVersion: v0.0.0
appReplicas: 1
containerPort: 8080
containerEnvVars:
- name: ENV_VAR_ONE
  value: "valueone"
- name: ENV_VAR_TWO
  value: "valuetwo"

servicePort: 8080
serviceType: ClusterIP
```

### Create Values for Each Microservice
- Create these outside of the Microservices directory for each Deployment we want to configure. We will copy and paste the values from our `config.yaml` from the previous project into each respective new values.yaml file.

```yaml
#email-service-values.yaml Example
appName: emailservice
appImage: gcr.io/google-samples/micrcoservices-demo/emailservice
appVersion: v0.8.0
appReplicas: 2
containerPort: 8080 #Left for transparency, already default in values.yaml
containerEnvVars:
- name: PORT
  value: "8080"

servicePort: 5000
#serviceType is default in values.yaml so we omit it
```

-Start Minikube locally
	- `minikube start`
- Verify this is configured correctly
	- `helm template -f email-service-values.yaml microservice`
	- This will print out the Deployment and Service for the Microservice if the syntax is correct and you can verify it says what it should.
	- We have a reference `config.yaml` and we can compare the command output to this file to verify.
- Install the Chart for emailservice
	- `helm install -f email-service-values.yaml emailservice microservice`
- Verify Helm Chart
	- `helm ls`
- Verify 2 Replica Pods for emailservice
	- `kubectl get pod`

![Successful Email Service Deploy](/images/m10-7-successful-deploy.png)

### Replicate Microservice Values Files
- We need to create a values YAML file for all remaining Microservices. We will use the `config.yaml` values and the previous `email-service-values.yaml` template.
- Once completed, we can now deploy another chart for a different service
	- `helm install -f values/recommendation-service-values.yaml recommendationservice microservice`

![Second Successful Deploy](/images/m10-7-second-successful-deploy.png)


### Create Redis Helm Chart
- Create new Chart
	- `helm create redis`
- Navigate to templates and delete all generated files. Create a new `deployment.yaml` and `service.yaml`
- Paste the `config.yaml` contents into their respective files, and remove the variables to placeholders.

```yaml
#Redis service.yaml template example
apiVersion: v1
kind: Service
metadata:
 name: {{ .Values.appName }}
spec:
 type: ClusterIP
 selector:
  app: {{ .Values.appName }}
 ports:
 - protocol: TCP
   port:{{ .Values.servicePort }}
   targetPort: {{ .Values.containerPort }}
```

- We will also add a `redis-values.yaml` to hold overridden values and store that alongside the microservice yaml files. There will only be two values inside.
	- `appName: redis-cart`
	- `appReplicas: 2`

- The default `values.yaml` file for Redis will contain the key/values that we have from the `config.yaml` and the placeholders for service and deployment files.
- Test our template
	- `helm template -f values/redis-values.yaml charts/redis`
- Perform a Dry-run
	- `helm install --dry-run -f values/redis-values.yaml rediscart charts/redis`

![Successful Dry Run](/images/m10-7-successful-dry-run.png)